IMAGE_NAME := adam.boscarino.me
PORT := 1337

SPACES_BUCKET := adam-boscarino-me
SPACES_ACCESS_KEY := ${SPACES_ACCESS_KEY_ID}
SPACES_SECRET_KEY := ${SPACES_SECRET_ACCESS_KEY}

release:
	@./release.sh

build-image:
	@echo "==> Building the docker image"
	@docker build --rm --force-rm -t $(IMAGE_NAME) .

docker-run: build-site build-image
	@echo "==> Running site"
	@docker run --rm -d \
		-v $(CURDIR):/usr/src/site \
		-p $(PORT):$(PORT)\
		--name site \
		$(IMAGE_NAME) hugo server --port=$(PORT) --bind=0.0.0.0

run: build-site
	@echo "==> Running site"
	@hugo server --port=$(PORT) --bind=0.0.0.0

build-site:
	hugo

.PHONY: build-image run docker-run build-site release
