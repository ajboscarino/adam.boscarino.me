#!/usr/bin/env sh
set -e
set -o pipefail

echo "Building site with hugo"
hugo

if [ ! -d public ]; then
  echo "Something went wrong we should have a public folder."
fi

echo "Entering public dir"
cd public

echo "Adding changes to git"
git add .

echo "Committing changes to git"
msg="rebuilding site `date`"
if [ $# -eq 1 ]
  then msg="$1"
fi
git commit -m "$msg"

echo "Pushing changes to Github"
git push origin master
