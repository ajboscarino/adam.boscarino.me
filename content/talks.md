+++
title = "Talks"
weight = 30
draft = false
+++

- NYC Data Engineering Meetup - Airflow at DigitalOcean
  - [[slides](https://github.com/ajbosco/talks/blob/master/nyc-data-eng-meetup/airflow-at-do.pdf)]