FROM alpine
MAINTAINER Adam Boscarino

RUN apk --no-cache add \
	ca-certificates \
	curl \
	tar \
	python3 \
	&& pip3 install s3cmd

ENV HUGO_VERSION 0.47.1
ENV HUGO_BINARY hugo
ENV HUGO_RESOURCE hugo_extended_${HUGO_VERSION}_Linux-64bit

ADD https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/${HUGO_RESOURCE}.tar.gz /tmp/

RUN  mkdir /tmp/${HUGO_RESOURCE} && tar -xvzf /tmp/${HUGO_RESOURCE}.tar.gz -C /tmp/${HUGO_RESOURCE}/ \
	&& mv /tmp/${HUGO_RESOURCE}/${HUGO_BINARY} /usr/local/bin/hugo && rm -rf /tmp/hugo*

WORKDIR /usr/src/site/