## adam.boscarino.me

Source for my personal site. This site is hosted on DigitalOcean Spaces.

### Deployment

To deploy run:

```
make release SPACES_ACCESS_KEY=$SPACES_ACCESS_KEY_ID SPACES_SECRET_KEY=$SPACES_SECRET_ACCESS_KEY
```
